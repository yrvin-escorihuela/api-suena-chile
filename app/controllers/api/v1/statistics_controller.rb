module Api
  module V1
    class Api::V1::StatisticsController < ApplicationController
      include ActionController::Serialization
      def playbacks_by_artist
        @result = Artist.all.select("name, sum(albums.playbacks) as total_playbacks").joins(:albums).group("artists.name").order("total_playbacks desc").limit(10)
        render json: StatisticsSerializer.new(@result).serializable_hash, status: :ok
      end
    end
  end
end


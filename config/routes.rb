Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
        get 'statistics/playbacks_by_artist', to: 'statistics#playbacks_by_artist'
        resources :artists do
          resources :albums
          get '/albums/:id/play', to: 'albums#play', as: :play
        end
    end
  end
end

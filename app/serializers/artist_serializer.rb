class ArtistSerializer < ActiveModel::Serializer
  attributes :id, :name, :genre, :albums
  def albums
    self.object.albums.map do |album|
      {album_title: album.title, album_release_year: album.year, album_duration: album.total_duration}
    end
  end
end

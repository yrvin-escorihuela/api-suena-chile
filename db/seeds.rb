# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Artists
Artist.create({name: "Violeta Parra", genre: "Folklórica"})
Artist.create({name: "Los Prisioneros", genre: "Rock"})
Artist.create({name: "La Ley", genre: "Pop"})

#Albums
Album.create({artist_id: 1, title: 'Cantos de Chile (Presente/Ausente)', year: 1956, total_duration: '59:42', playbacks: 0})
Album.create({artist_id: 2, title: 'La voz de los \'80', year: 1984, total_duration: '40:22', playbacks: 0})
Album.create({artist_id: 3, title: 'Vértigo', year: 1998, total_duration: '55:07', playbacks: 0})


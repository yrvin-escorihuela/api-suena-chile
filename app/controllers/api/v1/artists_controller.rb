module Api
  module V1
    class ArtistsController < ApplicationController
      include ActionController::Serialization
      before_action :set_artist, only: %i[show update destroy]

      # GET /artists
      def index
        @artists = Artist.all

        render json: @artists
      end

      # GET /artists/1
      def show
        if @artist
          render json: @artist
        else
          head 404
        end
      end

      # POST /artists
      def create
        @artist = Artist.new(artist_params)

        if @artist.save
          render json: @artist, status: :created
        else
          render json: @artist.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /artists/1
      def update
        if @artist.update(artist_params)
          render json: @artist
        else
          render json: @artist.errors, status: :unprocessable_entity
        end
      end

      # DELETE /artists/1
      def destroy
        if @artist
          @artist.destroy
          head 204
        else
          head 404
        end
      end
      private
        # Use callbacks to share common setup or constraints between actions.
        def set_artist
          @artist = Artist.find_by_id(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def artist_params
          params.require(:artist).permit(:name, :genre)
        end
    end
  end
end


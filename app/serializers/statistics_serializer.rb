class StatisticsSerializer
  def initialize(playbacks)
    @playbacks = playbacks
  end

  def serializable_hash
    @playbacks.map do |p|
      { artist_name: p.name, total_playbacks: p.total_playbacks }
    end
  end

  def as_json
    { statistics: serializable_hash }
  end

end

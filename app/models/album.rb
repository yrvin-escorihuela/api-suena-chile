class Album < ApplicationRecord
  belongs_to :artist
  validates :title, uniqueness: true

  def update_playback
    self.playbacks = self.playbacks + 1
    self.save
  end
end

class AlbumSerializer < ActiveModel::Serializer
  attributes :id, :title, :year, :total_duration, :playbacks, :artist
  def artist
    {artist_id: self.object.artist.id, artist_name: self.object.artist.name, artist_genre: self.object.artist.genre}
  end
end

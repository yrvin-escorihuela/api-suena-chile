# API Suena Chile:
### CRUD de artistas musicales chilenos y sus respectivas obras.

##### Requerimientos mínimos:
- Ruby 2.5.5
- Rails 5.2.3
Para la gestión de la versión de Ruby y Rails se ha utilizado **RVM** pero se puede utilizar **rbenv** si es de su preferencia.

Pasos para la ejecución:

1. Clonar el repositorio.
2. Estando en la cónsola de su preferencia (iTerm, Terminal, etc) y con el intérprete de su preferencia (bash, zsh, etc), ejecutar **bundle install¨**
3. Posterior a eso es necesario ejecutar las migraciones para la base de datos, en este caso se ocupó **SQLite** por temas de simplicidad y prototipado. Antes de ello es necesario crear la base de datos, por medio del comando **rake db:create**. Luego para crear las tablas a través de las migraciones, se ejecuta el comando **rake db:migrate**
4. Al tener ya esto, puede proceder a cargar datos predefinidos (seeds) en la base de datos por si desea ver resultados inmediatos en la aplicación web, debe ejecutar el comando **rake db:seed**
5. Al tener datos cargados, puede ejecutar el servidor de prueba a partir del comando **rails s -p3000** en la cónsola, se hace énfasis en el puerto 3000, dado que es por ese puerto que se consume la API desde la web, por defecto también el servidor de prueba utiliza ese mismo puerto, puede obviar **-p3000** si lo desea.
6. Todos los endpoints mostrados abajo, deberán poseer el prefijo **http://localhost:3000** o **http://127.0.0.1:3000** cualquiera que sea su preferencia para ejecutar las peticiones HTTP.

### Endpoints Artistas
Se ha podido utilizar swagger para esto, sin embargo procederé a utilizar markdown para hacer una descripción rápida de cada endpoint y su utilización. También se recomienda utilizar Postman para la verificación de estos endpoints, pero puede utilizar algun otro cliente de su preferencia: HTTPie por ejemplo.

- Endpoint: **/api/v1/artists**
- Método HTTP: **GET**
- Descripción: Ver todos los artistas con todos los albumes y sus datos relacionados.
- Datos de salida:
```json
[
    {
        "id": 1,
        "name": "Los Prisioneros",
        "genre": "Rock",
        "albums": [
            {
                "album_title": "La voz de los 80's",
                "album_release_year": 1989,
                "album_duration": "50:00"
            }
        ]
    },
    {
        "id": 2,
        "name": "La Ley",
        "genre": "Pop",
        "albums": [
            {
                "album_title": "Vértigo",
                "album_release_year": 1998,
                "album_duration": "50:00"
            }
        ]
    },
    {
        "id": 3,
        "name": "Violeta Parra",
        "genre": "Pop",
        "albums": [
            {
                "album_title": "Cantos de Chile (Presente/Ausente)",
                "album_release_year": 1956,
                "album_duration": "59:42"
            }
        ]
    }
]
```
***
- Endpoint: **/api/v1/artists/:id**
- Método HTTP: **GET**
- Descripción: Ver un único artista con todos los albumes y sus datos relacionados.
- Datos de Salida: 
```json
{
    "id": 1,
    "name": "Los Prisioneros",
    "genre": "Rock",
    "albums": [
        {
            "album_title": "La voz de los 80's",
            "album_release_year": 1989,
            "album_duration": "50:00"
        }
    ]
}
```
***
- Endpoint: **/api/v1/artists/**
- Método HTTP: **POST**
- Descripción: Dar de alta un nuevo artista.
- Datos de entrada: 
```json
{
    "name": "La Combo Tortuga",
    "genre": "Cumbia"{
    "id": 5,
    "name": "La Combo Tortuga",
    "genre": "Cumbia",
    "albums": []
}
}
```
- Datos de Salida:
```json
{
    "id": 5,
    "name": "La Combo Tortuga",
    "genre": "Cumbia",
    "albums": []
}
```
***
- Endpoint: **/api/v1/artists/:id**
- Método HTTP: **PUT**
- Descripción: Editar los datos del artista.
- Datos de entrada: 
```json
{
    "name": "La Combo Tortuga",
    "genre": "Cumbia"
}
```
- Datos de Salida:
```json
{
    "id": 5,
    "name": "Chico Trujillo",
    "genre": "Bolero",
    "albums": []
}
```
***
- Endpoint: **/api/v1/artists/:id**
- Método HTTP: **DELETE**
- Descripción: Borrar un único artista con todos los albumes y sus datos relacionados.
- Datos de Salida: 
```json
{
    "id": 1,
    "name": "Los Prisioneros",
    "genre": "Rock",
    "albums": [
        {
            "album_title": "La voz de los 80's",
            "album_release_year": 1989,
            "album_duration": "50:00"
        }
    ]
}
```
***

### Endpoints Albumes
- Endpoint: **/api/v1/artists/:artist_id/albums/**
- Método HTTP: **GET**
- Descripción: Ver todos los albumes de un artista.
- Datos de salida:
```json
[
    {
        "id": 1,
        "title": "La voz de los 80's",
        "year": 1989,
        "total_duration": "50:00",
        "playbacks": 17,
        "artist": {
            "artist_id": 1,
            "artist_name": "Los Prisioneros",
            "artist_genre": "Rock"
        }
    }
]
```
***
- Endpoint: **/api/v1/artists/:artist_id/albums/:id**
- Método HTTP: **GET**
- Descripción: Ver un album de un artista.
- Datos de salida:
```json
[
    {
        "id": 1,
        "title": "La voz de los '80",
        "year": 1984,
        "total_duration": "50:00",
        "playbacks": 17,
        "artist": {
            "artist_id": 1,
            "artist_name": "Los Prisioneros",
            "artist_genre": "Rock"
        }
    }
]
```
***
- Endpoint: **/api/v1/artists/:artist_id/albums/**
- Método HTTP: **POST**
- Descripción: Dar de alta un album de un artista.
- Datos de entrada:
```json
{
    "title": "Cantos de Chile (Presente/Ausente)",
    "year": 1956,
    "total_duration": "59:42",
    "playbacks": 0
    
}
```
- Datos de salida:
```json
{
    "id": 3,
    "title": "Cantos de Chile (Presente/Ausente)",
    "year": 1956,
    "total_duration": "59:42",
    "playbacks": 0,
    "artist": {
        "artist_id": 3,
        "artist_name": "Violeta Parra",
        "artist_genre": "Folklórica"
    }
}
```
***
- Endpoint: **/api/v1/artists/:artist_id/albums/:id**
- Método HTTP: **PUT**
- Descripción: Dar de alta un album de un artista.
- Datos de entrada:
```json
{
    "title": "Cantos de Chile",
    "year": 1956,
    "total_duration": "59:42",
}
```
- Datos de salida:
```json
{
    "id": 3,
    "title": "Cantos de Chile",
    "year": 1956,
    "total_duration": "59:42",
    "playbacks": 0,
    "artist": {
        "artist_id": 3,
        "artist_name": "Violeta Parra",
        "artist_genre": "Folklórica"
    }
}
```
***
- Endpoint: **/api/v1/artists/:artist_id/albums/:id**
- Método HTTP: **DELETE**
- Descripción: Dar de baja un album de un artista.
***

### Endpoint Estadísticas
Este endpoint está limitado a mostrar un arreglo de 10 objetos json (artistas y con sus reproducciones), dado a que la web para mostrar solo 10 artistas.
- Endpoint: **/api/v1/statistics/playbacks_by_artist**
- Método HTTP: **GET**
- Descripción: Mostrar cada artista con el total de reproducciones de sus discos.
- Datos de salida:
```json
[
    {
        "artist_name": "Los Prisioneros",
        "total_playbacks": 17
    },
    {
        "artist_name": "La Ley",
        "total_playbacks": 14
    }
]
```
### Endpoint Reproducción
- Endpoint: **/api/v1/artists/1/albums/1/play**
- Método HTTP: **GET**
- Descripción: Simular la reproducción de un album de algun artista.
- Datos de salida:
```json
{
    "id": 1,
    "title": "La voz de los 80's",
    "year": 1989,
    "total_duration": "50:00",
    "playbacks": 19,
    "artist": {
        "artist_id": 1,
        "artist_name": "Los Prisioneros",
        "artist_genre": "Rock"
    }
}
```

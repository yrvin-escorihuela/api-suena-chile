module Api
  module V1
    class AlbumsController < ApplicationController
      include ActionController::Serialization
      before_action :set_artist, only: %i[index create]
      before_action :set_album, only: %i[show update destroy play]
      # GET /albums
      def index
        render json: @artist.albums.all
      end

      # GET /albums/1
      def show
        if @album
          render json: @album
        else
          head 404
        end
      end

      # POST /albums
      def create
        @album = @artist.albums.build(album_params)

        if @album.save
          render json: @album, status: :created
        else
          render json: @album.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /albums/1
      def update
        if @album
          if @album.update(album_params)
            render json: @album
          else
            render json: @album.errors, status: :unprocessable_entity
          end
        else
          head :not_found
        end

      end

      # DELETE /albums/1
      def destroy
        if @album
          @album.destroy
          head 204
        else
          head :not_found
        end
      end

      def play
        if @album
          @album.update_playback
          render json: @album, status: 200
        else
          head 404
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_album
          @album = Album.find_by_id(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def album_params
          params.require(:album).permit(:title, :total_duration, :year, :playbacks, :artist_id)
        end
        def set_artist
          @artist = Artist.find_by_id(params[:artist_id])
        end
    end
  end
end
